#ifndef __IO_CFG_H__
#define __IO_CFG_H__

#ifdef __cplusplus
extern "C"
{
#endif
#include "stm32f0xx_gpio.h"
#include <stdint.h>

#include "port.h"
#include "platform.h"
#include "stm32f0xx.h"
//#include "stm32f0xx_rcc.h"
//#include "stm32f0xx_gpio.h"
//#include "stm32f0xx_tim.h"
//#include "stm32f0xx_exti.h"

#define LED_RUN_IO_PIN          (GPIO_Pin_0)
#define LED_RUN_IO_PORT			(GPIOA)
#define LED_RUN_IO_CLOCK		(RCC_AHBPeriph_GPIOA)

/*****************************************************************************
 *Pin map step motor1
******************************************************************************/
#define STEP_MOTOR1_EN_PIN					(GPIO_Pin_2)
#define STEP_MOTOR1_EN_PORT					(GPIOA)
#define STEP_MOTOR1_EN_CLOCK				(RCC_AHBPeriph_GPIOA)

#define STEP_MOTOR1_DIR_PIN					(GPIO_Pin_3)
#define STEP_MOTOR1_DIR_PORT				(GPIOA)
#define STEP_MOTOR1_DIR_CLOCK				(RCC_AHBPeriph_GPIOA)

#define STEP_MOTOR1_PULSE_PIN				(GPIO_Pin_1)
#define STEP_MOTOR1_PULSE_PORT				(GPIOB)
#define STEP_MOTOR1_PULSE_CLOCK				(RCC_AHBPeriph_GPIOB)

typedef union {
	struct {
		uint8_t bit1		: 1;
		uint8_t bit2		: 1;
		uint8_t bit3		: 1;
		uint8_t bit4		: 1;
		uint8_t reserved	: 4;
	} set;
	uint8_t val;
} io_dip_switch_u;

extern void		io_dip_switch_init();
extern uint16_t	io_dip_switch_get();
extern void		io_button_init();
extern uint8_t	io_button_read();

extern void io_led_status_init();
extern void io_led_status_on();
extern void io_led_status_off();

extern void uart1_init(uint32_t baud);
extern void uart1_putc(uint8_t c);

extern void io_independent_watchdog_init();
extern void io_independent_watchdog_reset();

extern void init_led();
extern void io_led_on();
extern void io_led_off();

extern void TIM_Config(void);
extern void step_motor1_init();
extern void init_button();

extern void EXTI0_Config(void);

#ifdef __cplusplus
}
#endif

#endif //__IO_CFG_H__
