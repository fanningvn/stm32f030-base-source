#ifndef __YF_X201B_H__
#define __YF_X201B_H__

#if defined (__cplusplus)
extern "C" {
#endif

#include <stdint.h>
#include <math.h>
#include <stdio.h>

typedef struct {
    unsigned long pulse_freq;
    unsigned long pulse_freq_prev;
    unsigned long pulse_freq_curr;
    unsigned long tick_ms;
    unsigned long water_flow_in_hour; //flowrate in L/hour
} yf_s201b_t;

void yf_s201b_cal(yf_s201b_t* yf_s201b);

#ifdef __cplusplus
}
#endif

#endif //__YF_X201B_H__
