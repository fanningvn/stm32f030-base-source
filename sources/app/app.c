#include <string.h>

#include "app.h"
#include "app_data.h"
#include "platform.h"
#include "stm32f0xx_wwdg.h"
#include "stm32f0xx_exti.h"
#include "stm32f0xx_syscfg.h"

#include "io_cfg.h"
#include "xprintf.h"
#include "button.h"
#include "led.h"

#include "mb.h"
#include "mbport.h"
#include "port.h"
#include "math.h"

#include "yf_x201b.h"

#define REG_HOLDING_START		1
#define REG_HOLDING_NREGS		200

#define BUTTON_CONFIG_ID		1

#define EVENT_CONFIG_INIT		1

yf_s201b_t yf_s201b;

void systick_handler() {
    yf_s201b_cal(&yf_s201b);
}

void ext0_yf_s201b_handler() {
    yf_s201b.pulse_freq++;
}

void main_app() {
    init_led();
    EXTI0_Config();
    uart1_init(115200);

    if (SysTick_Config(SystemCoreClock / 1000)) {
        /* Capture error */
        while (1);
    }

    while (1) {
        DelayMicroSeconds(1000000);
        xprintf("pulse_freq        : %d\n", yf_s201b.pulse_freq);
        xprintf("water_flow_in_hour: %d flowrate in L/hour\n", yf_s201b.water_flow_in_hour);
    }
}

/* ----------------------- Static Function ---------------------------------*/
void app_dbg_fatal( const int8_t* s, uint8_t c ) {
    (void)s;
    (void)c;
    sys_ctrl_reset();
}
