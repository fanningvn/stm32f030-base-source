#include "port.h"

#include "yf_x201b.h"
#include "xprintf.h"

void yf_s201b_cal(yf_s201b_t* yf_s201b) {
    yf_s201b->tick_ms++;
    if (yf_s201b->tick_ms == 1000) {

        yf_s201b->tick_ms = 0;
        yf_s201b->pulse_freq_curr = yf_s201b->pulse_freq;
        yf_s201b->water_flow_in_hour = ((yf_s201b->pulse_freq_curr - yf_s201b->pulse_freq_prev) * 60 / 7.5); // (Pulse frequency x 60 min) / 7.5Q = flowrate in L/hour
        yf_s201b->pulse_freq_prev = yf_s201b->pulse_freq_curr;
    }
}
