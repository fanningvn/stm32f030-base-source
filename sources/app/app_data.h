#ifndef __APP_FLASH_H__
#define __APP_FLASH_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

#include "mb.h"
#include "mbport.h"

#include "stm32f0xx.h"
#include "stm32f0xx_conf.h"

#define FLASH_PAGE_SIZE			((uint32_t)0x00000400)   /* FLASH Page Size */
#define FLASH_USER_START_ADDR	((uint32_t)0x08006000)   /* Start @ of user Flash area */
#define FLASH_USER_END_ADDR		((uint32_t)0x08007000)   /* End @ of user Flash area */

#define BAUDRATE_OPTION_SIZE	( 10 )
#define SW_OPTION_SIZE			( 2 )
#define ADDRESS_SIZE			( 255 )

typedef struct {
	eMBMode Mode;
	BOOL IsSwitchAddress;
	uint32_t SlaveAddress;
	UCHAR Port;
	uint32_t BaudRate;
	eMBParity Parity;
	eMBDatabits Databits;
	eMBStopbits Stopbits;
	int8_t TempCorrection;
	int8_t HumiCorrection;
} hw_modbus_config_t;

extern uint32_t baudrate_option[BAUDRATE_OPTION_SIZE];

extern hw_modbus_config_t hw_modbus_config_default;

extern uint8_t internal_flash_write_firstpage(uint8_t* data, uint32_t len);
extern void internal_flash_read_firstpage(uint8_t* data, uint32_t len);

#ifdef __cplusplus
}
#endif

#endif //__APP_FLASH_H__
